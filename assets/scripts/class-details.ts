const {ccclass, property} = cc._decorator;

@ccclass
export default class ClassDetails extends cc.Component {

	@property(cc.Label)
	className: cc.Label = null;

	@property(cc.Label)
	skillName1: cc.Label = null;

	@property(cc.Label)
	skillDescription1: cc.Label = null;

	@property(cc.Label)
	skillName2: cc.Label = null;

	@property(cc.Label)
	skillDescription2: cc.Label = null;

	public selectedClass: string = "";

	private skills = [
		{
			"name":"defender",
			"skill1": {"skillName1":"Defender Skill Name 1", "skillDescription1":"Defender Skill Description 1"},
			"skill2": {"skillName2":"Defender Skill Name 2", "skillDescription2":"Defender Skill Description 2"}
		},
		{
			"name":"scout",
			"skill1": {"skillName1":"Scout Skill Name 1", "skillDescription1":"Scout Skill Description 1"},
			"skill2": {"skillName2":"Scout Skill Name 2", "skillDescription2":"Scout Skill Description 2"}
		},
		{
			"name":"medic",
			"skill1": {"skillName1":"Medic Skill Name 1", "skillDescription1":"Medic Skill Description 1"},
			"skill2": {"skillName2":"Medic Skill Name 2", "skillDescription2":"Medic Skill Description 2"}
		},
		{
			"name":"clown",
			"skill1": {"skillName1":"Clown Skill Name 1", "skillDescription1":"Clown Skill Description 1"},
			"skill2": {"skillName2":"Clown Skill Name 2", "skillDescription2":"Clown Skill Description 2"}
		},
		{
			"name":"ninja",
			"skill1": {"skillName1":"Ninja Skill Name 1", "skillDescription1":"Ninja Skill Description 1"},
			"skill2": {"skillName2":"Ninja Skill Name 2", "skillDescription2":"Ninja Skill Description 2"}
		},
		{
			"name":"mechanic",
			"skill1": {"skillName1":"Mechanic Skill Name 1", "skillDescription1":"Mechanic Skill Description 1"},
			"skill2": {"skillName2":"Mechanic Skill Name 2", "skillDescription2":"Mechanic Skill Description 2"}
		}
	]

	renderData(selectedClass: string) {
		this.className.string = `${selectedClass}`;
		for (let i = 0; i < this.skills.length; i++) {
			if (selectedClass == this.skills[i].name) {
				this.skillName1.string = `${this.skills[i].skill1.skillName1}`;
				this.skillDescription1.string = `${this.skills[i].skill1.skillDescription1}`;
				this.skillName2.string = `${this.skills[i].skill2.skillName2}`;
				this.skillDescription2.string = `${this.skills[i].skill2.skillDescription2}`;
			}	
		}
	}
}
