import ClassDetails from "./class-details";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Main extends cc.Component {

	@property(cc.Node)
	logo: cc.Node = null;

	@property(cc.Node)
	classes: cc.Node = null;

	@property(cc.Node)
	classesChar: cc.Node = null;

	@property(ClassDetails)
	classDetails: ClassDetails = null;

	@property(cc.Node)
	weaponSelection: cc.Node = null;

	@property(cc.Node)
	downloadNow: cc.Node = null;

	onEnable() {	
		cc.tween(this.logo)
    .to(1, { position: cc.v3(790, 450) }, { easing: 'bounceIn'})
		.start()

		for (let i = 1; i < this.classesChar.children.length; i++) {
			this.classesChar.children[i].active = false;
		}

		this.classDetails.node.active = false;
		this.weaponSelection.active = false;
	}

	onSelectClass(event, data) {
		this.reset();
		this.classDetails.node.active = true;
		for (let i = 0; i < this.classesChar.children.length; i++) {
			if (data == this.classesChar.children[i].name) {
				this.classesChar.children[i].active = true;
				this.classDetails.renderData(data);
				this.classes.children[i].getComponentInChildren(cc.Button).node.active = true;
			}
		}
	}

	onConfirmSeletedClass() {
		this.classDetails.node.active = false;
		this.weaponSelection.active = true;
	}

	onConfirmSeletedWeapon() {
		this.downloadNow.active = true;
	}

	onClickDownloadNow() {
		cc.sys.openURL("https://play.google.com/store/apps/details?id=com.vng.g6.a.zombie&hl=en&gl=US")
	}

	reset() {
		for (let i = 0; i < this.classesChar.children.length; i++) {
			this.classesChar.children[i].active = false;
			this.classes.children[i].getComponentInChildren(cc.Button).node.active = false;
		}
	}
}
