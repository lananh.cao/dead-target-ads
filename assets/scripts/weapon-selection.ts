const {ccclass, property} = cc._decorator;

@ccclass
export default class WeaponSelection extends cc.Component {

	@property([cc.Node])
	weaponList: cc.Node[] = [];

	@property(cc.Label)
	weaponName: cc.Label = null;

	@property(cc.ProgressBar)
	damage: cc.ProgressBar = null;

	@property(cc.ProgressBar)
	accuracy: cc.ProgressBar = null;

	@property(cc.ProgressBar)
	range: cc.ProgressBar = null;

	@property(cc.ProgressBar)
	fireRate: cc.ProgressBar = null;

	@property(cc.ProgressBar)
	mobility: cc.ProgressBar = null;

	@property(cc.Button)
	nextButton: cc.Button = null;

	@property(cc.Button)
	prevButton: cc.Button = null;

	private weapons = [
		{
			"name":"M4",
			"damage":45,
			"accuracy":70,
			"range":45,
			"fireRate":60,
			"mobility":60
		},
		{
			"name":"PDW-57",
			"damage":90,
			"accuracy":40,
			"range":25,
			"fireRate":50,
			"mobility":75
		},
		{
			"name":"Type 25",
			"damage":55,
			"accuracy":48,
			"range":37,
			"fireRate":70,
			"mobility":60
		}
	]

	private weaponIndex: number = 0;

	onEnable() {
		this.reset();
		this.renderWeaponInfo(this.weaponIndex);
		cc.log(this.weaponIndex)
	}

	renderWeaponInfo(weaponIdx) {
		this.weaponName.string = `${this.weapons[weaponIdx].name}`;
		this.damage.progress = this.weapons[weaponIdx].damage / 100;
		this.accuracy.progress = this.weapons[weaponIdx].accuracy / 100;
		this.range.progress = this.weapons[weaponIdx].range / 100;
		this.fireRate.progress = this.weapons[weaponIdx].fireRate / 100;
		this.mobility.progress = this.weapons[weaponIdx].mobility / 100;
	}

	onChangeWeapon() {
		this.weaponList.forEach((weapon, index) => {
			if (index != this.weaponIndex) {
				weapon.active = false;
			}
		})
		
		this.weaponList[this.weaponIndex].active = true;
	}

	onClickNextBtn() {
		cc.log("next")
		if (this.weaponIndex == 2) {
			this.weaponIndex = 0
		} else {
			this.weaponIndex++;
		}
		this.renderWeaponInfo(this.weaponIndex);
		this.onChangeWeapon();
	}
	
	onClickPrevBtn() {
		cc.log("prev")
		if (this.weaponIndex == 0) {
			this.weaponIndex = 2
		} else {
			this.weaponIndex--;
		}
		this.renderWeaponInfo(this.weaponIndex);
		this.onChangeWeapon();
	}

	reset() {
		this.weaponIndex = 0;

		this.damage.progress = 0;
		this.accuracy.progress = 0;
		this.range.progress = 0;
		this.fireRate.progress = 0;
		this.mobility.progress = 0;
	}
}
